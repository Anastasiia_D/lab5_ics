FROM python:3.9

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

CMD [ "python", "./simple_python_app/app.py" ]