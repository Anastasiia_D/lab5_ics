import sqlite3
import mysql.connector

def setup_sqlite_db(db_name):
    connector = sqlite3.connect(f"{db_name}.db")
    mydb = connector.cursor()
    mydb.execute(f"CREATE TABLE IF NOT EXISTS Students (id INTEGER PRIMARY KEY, first_name VARACHAR, last_name VARACHAR, middle_name VARACHAR, email VARACHAR);")

def setup_mysql_db(**kwargs):
    config = dict(kwargs.items())
    connector = mysql.connector.connect(user=config.get('user'), password=config.get('password'),
                                  host=config.get('hostname'),
                                  port=config.get('port')
                                  )
    cursor = connector.cursor()
    cursor.execute(f"CREATE DATABASE IF NOT EXISTS {config.get('db_name')};")
    cursor.close()
    setup_mysql_table(config)

def setup_mysql_table(config):
    connector = mysql.connector.connect(user=config.get('user'), password=config.get('password'),
                                  host=config.get('hostname'),
                                  port=config.get('port'),
                                  database=config.get('db_name')
                                  )
    cursor = connector.cursor()
    cursor.execute(f"CREATE TABLE IF NOT EXISTS Students (id int NOT NULL AUTO_INCREMENT, first_name varchar(255) NOT NULL, last_name varchar(255) NOT NULL, middle_name varchar(255) NOT NULL, email varchar(255) NOT NULL, PRIMARY KEY (id));")
    cursor.close()